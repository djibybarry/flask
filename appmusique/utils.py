from .app import db, login_manager
from .models import *
from sqlalchemy import update
from flask import flash, redirect, url_for

def get_albums(range=10):
    return Album.query.limit(range).all()

def get_album(id):
    return Album.query.get(id)

def get_artist_name(artid):
    return Author.query.get(artid).getName()

def get_artist_albums(artid):
	return Album.query.filter(Album._author_id == artid).all()

def get_genres_album(id):
	assocs = AssocGenreAlbum.query.filter(AssocGenreAlbum._album_id == id).all()
	genres = []
	for a in assocs:
		genres.append(a.getGenre())
	return genres

def search_artist_from_name(name):
	return Author.query.filter(Author._name.like("%" + name + "%")).all()

def search_album_from_name(name):
	return Album.query.filter(Album._title.like("%" + name + "%")).all()

def search_user_from_name(name):
	return User.query.filter(User._nickname.like("%" + name + "%")).all()

def search_playlist_from_name(name):
	return #Playlist.query.filter(Playlist._name.like("%" + name + "%")).all()

def register_new_user(nickname, password):
	user = User(_nickname = nickname, _password = password, _level = u'member')
	db.session.add(user)
	db.session.commit()

def modify_user_impl(id, npass):
	user = User.query.filter(User._id == id).first()
	if user is None:
		flash('User does not exists' , 'error')
		flash('Cannot modify credentials' , 'error')
		return redirect(url_for('home'))

	if npass is None or npass == "":
		flash('Bad data' , 'error')
		flash('Cannot modify credentials' , 'error')
		return redirect(url_for('home'))

	flash('id = ' + str(id) + ' npass = ' + npass, 'warning')

	#update(User).where(User._id == id).values(_password = npass)
	db.session.query(User).filter(User._id == id).update({'_password': npass})
	db.session.commit()
	flash('Credentials modified successfully')
	return redirect(url_for('usercp'))

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))
