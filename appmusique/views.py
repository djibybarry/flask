from .app import app
from flask import Flask,session, request, flash, url_for, redirect, render_template, abort ,g
from flask.ext.login import login_user , logout_user , current_user , login_required
from .models import *
from .utils import *

# home route, default page with samples
@app.route("/")
def home():
	return render_template("frontpage.html",
	title = "iTunes",
	albums = get_albums(20))

# albums samples
@app.route("/album/")
@app.route("/albums/")
def albums():
	return "<h1>albums</h1>"

# album profile
@app.route("/album/<int:id>/")
def album(id = -1):
	return render_template("album.html",
	title = get_album(id).getTitle(),
	album = get_album(id),
	genres = get_genres_album(id))

# artists sample
@app.route("/artist/")
@app.route("/artists/")
def artists():
	return "<h1>artists</h1>"

# artist profile
@app.route("/artist/<int:id>/")
def artist(id = -1):
	return render_template("frontpage.html",
	title = get_artist_name(id),
	albums = get_artist_albums(id),
	name = get_artist_name(id))

# search
@app.route("/search/", methods=['POST'])
def search():
	rawquery = request.form['query']

	return render_template("search.html",
	arts = search_artist_from_name(rawquery),
	albs = search_album_from_name(rawquery),
	usrs = search_user_from_name(rawquery),
	plts = search_playlist_from_name(rawquery))

# default add choice
@app.route("/add/")
@login_required
def add():
	return "<h1>add</h1>"

# add artist form
@app.route("/add/artist/")
@login_required
def add_artist():
	return "<h1>add_artist</h1>"

# add artist script
@app.route("/add/artist/<int:id>")
@login_required
def add_artist_script(id = None):
	return "<h1>add_artist %d</h1>" % (id)

# add album form
@app.route("/add/album/")
@login_required
def add_album():
	return "<h1>add_album</h1>"

# add artists script
@app.route("/add/album/<int:id>")
@login_required
def add_album_script(id = None):
	return "<h1>add_album %d</h1>" % (id)

# default remove
@app.route("/remove/")
@login_required
def remove():
	return "<h1>remove list %d</h1>"

# modify artist
@app.route("/modify/artist/<int:id>/")
@login_required
def modify_artist(id = None):
	return "<h1>modify_artist %d</h1>" % (id)

# modify album
@app.route("/modify/album/<int:id>/")
@login_required
def modify_album(id = None):
	return "<h1>modify_album %d</h1>" % (id)

# modify user credentials
@app.route("/modify/user/", methods=['POST'])
@login_required
def modify_user():
	return modify_user_impl(current_user._id, request.form['newpassword'])

# remove artist
@app.route("/remove/artist/<int:id>/")
@login_required
def remove_artist(id = None):
	return "<h1>remove_artist %d</h1>" % (id)

# remove album
@app.route("/remove/album/<int:id>/")
@login_required
def remove_album(id = None):
	return "<h1>remove_album %d</h1>" % (id)

# list of users
@app.route("/users/")
def users():
	return "<h1>users</h1>"

# current user control panel/profile
@app.route("/user/")
@login_required
def usercp():
	return render_template("usercp.html", cuser = current_user)

# current user playlists management
@app.route("/user/playlists/")
@app.route("/user/playlist/")
@login_required
def playlists():
	return "<h1>list of public playlists</h1>"

# current user playlist y view
@app.route("/user/playlist/<int:pid>")
@login_required
def playlist(pid = None):
	return "<h1>playlist %d</h1>" % (pid)

# user x profile
@app.route("/user/<int:uid>/")
@login_required
def user(uid = None):
	return "<h1>user %d</h1>" % (uid)

# user x playlists view
@app.route("/user/<int:uid>/playlists")
@app.route("/user/<int:uid>/playlist")
def user_playlist(uid = None):
	return "<h1>list of user's playlists</h1>"

# user x playlist y view
@app.route("/user/<int:uid>/playlist/<int:pid>")
def user_playlist_view(uid = None, pid = None):
	return "<h1>user %d, playlist %d</h1>" % (uid, pid)

# register form
@app.route("/register/")
def register():
	return render_template("register.html", title = "Register")

# register script
@app.route("/registering/", methods=['POST'])
def registering():
	register_new_user(request.form['nickname'], request.form['password'])
	return redirect(url_for('home'))

# login form
@app.route("/login/")
def login():
	return render_template("login.html", title = "Login")

# login script, data in post
@app.route("/login/in/", methods=['POST'])
def login_in():
	nickname = request.form['nickname']
	password = request.form['password']

	registered_user = User.query.filter_by(_nickname = nickname, _password = password).first()
	if registered_user is None:
		flash('Username or Password is invalid' , 'error')
		return redirect(url_for('login'))
	login_user(registered_user)
	flash('Logged in successfully')
	return redirect(url_for('home'))

# logout
@app.route("/logout/")
@login_required
def logout():
	logout_user()
	flash('Logged out successfully')
	return redirect(url_for('home'))

# unauthorized handler
@login_manager.unauthorized_handler
def unauthorized():
	next_ = flask.request.args.get('next')
	return redirect(next_ or url_for('login'))
