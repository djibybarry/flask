from .app import db
import flask
from sqlalchemy_utils import *
from sqlalchemy import *
from sqlalchemy.orm import relationship
#from sqlalchemy.ext.declarative import declarative_base

class AssocGenreAlbum(db.Model):
	__tablename__ = 'assoc_genre_album'
	_album_id = db.Column(Integer, ForeignKey('album._id'), primary_key=True)
	_genre_id = db.Column(Integer, ForeignKey('genre._id'), primary_key=True)
	_album = db.relationship('Album', backref = db.backref('a2g', lazy = 'dynamic'))
	_genre = db.relationship('Genre', backref = db.backref('g2a', lazy = 'dynamic'))

	def getAlbum(self):
		return self._album

	def getGenre(self):
		return self._genre

class AssocPlaylistAlbum(db.Model):
	__tablename__ = 'assoc_playlist_album'
	_album_id = db.Column(Integer, ForeignKey('album._id'), primary_key=True)
	_playlist_id = db.Column(Integer, ForeignKey('playlist._id'), primary_key=True)
	_album = db.relationship('Album', backref = db.backref('a2p', lazy = 'dynamic'))
	_playlist = db.relationship('Playlist', backref = db.backref('p2a', lazy = 'dynamic'))

	def getAlbum(self):
		return self._album

	def getPlaylist(self):
		return self._playlist

class Author(db.Model):
	_id = db.Column(db.Integer, primary_key = True)
	_name = db.Column(db.String(100))

	def __repr__(self):
		return 'Author(' + self._id + ', ' + self._name + ')'

	def getId(self):
		return self._id

	def getName(self):
		return self._name

class Genre(db.Model):
	__tablename__ = 'genre'
	_id = db.Column(db.Integer, primary_key = True)
	_name = db.Column(db.String(100))

	def __repr__(self):
		return 'Genre(' + self._id + ', ' + self._name + ')'

	def getId(self):
		return self._id

	def getName(self):
		return self._name

class Album(db.Model):
	__tablename__ = 'album'
	_id = db.Column(db.Integer, primary_key = True)
	_title = db.Column(db.String(100))
	_year = db.Column(db.Integer)
	_img = db.Column(db.String(255))
	_parent = db.Column(db.String(100))
	_author_id = db.Column(db.Integer, db.ForeignKey('author._id'))
	_author = db.relationship('Author', backref = db.backref('albums', lazy = 'dynamic'))

	def __repr__(self):
		return 'Album(' + self._id + ', ' + self._title + ', ' + self._year + ', ' + self._img + ', ' + self._parent + ')'

	def getId(self):
		return self._id

	def getTitle(self):
		return self._title

	def getYear(self):
		return self._year

	def getImgPath(self):
		return self._img

	def getParent(self):
		return self._parent

	def getAuthor(self):
		return self._author

	def getAuthorId(self):
		return self._author_id

class Playlist(db.Model):
	__tablename__ = 'playlist'
	_id = db.Column(db.Integer, primary_key = True)
	_name = db.Column(db.String(100))
	_user_id = db.Column(db.Integer, db.ForeignKey('user._id'))
	_user = db.relationship('User', backref = db.backref('users', lazy = 'dynamic'))

	def getId(self):
		return self._id

	def getName(self):
		return self._name

	def getUserId(self):
		return self._user_id

	def getUser(self):
		return self._user

	def getAlbums(self):
		return self._albums

class User(db.Model):
	LEVEL = [
        (u'admin', u'Admin'),
        (u'member', u'Member')
    ]

	_id = db.Column(db.Integer, primary_key = True)
	_nickname = db.Column(db.String(15), unique = True, nullable = False)
	_password = db.Column(db.String(15), unique = False, nullable = False)
	#_password = db.Column(PasswordType(schemes = ['pbkdf2_sha512']), unique = False, nullable = False)
	_level = db.Column(ChoiceType(LEVEL))

	def __repr__(self):
		return 'user(' + self._id + ', ' + self._nickname + ')'

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return str(self._id)

	def getId():
		return self._id

	def getNickname():
		return self._nickname

	def getPassword():
		return self._password

	def getLevel():
		return self._level
