import os.path
def mkpath(p):
	return os.path.normpath(os.path.join(os.path.dirname(__file__), p))

from flask import Flask
app = Flask(__name__)
app.debug = True

from flask.ext.script import Manager
manager = Manager(app)

from flask.ext.bootstrap import Bootstrap
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap(app)

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///' + mkpath('../myapp.db'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = "1058a32f-327f-48a3-9d3f-9c5131441c10"

from flask.ext.login import LoginManager
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "Login"
login_manager.login_message = "Veuillez vous authentifier."
login_manager.login_message_category = "Informations"
